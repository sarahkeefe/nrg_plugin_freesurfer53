package org.nrg.xnat.freesurfer53;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_freesurfer53", name = "XNAT 1.7 FreeSurfer 53 Plugin", description = "This is the XNAT 1.7 FreeSurfer 53 Plugin.")
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class Freesurfer53Plugin {
}