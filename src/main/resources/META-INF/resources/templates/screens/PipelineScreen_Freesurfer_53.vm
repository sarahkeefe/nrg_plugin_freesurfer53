##Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
#set ($template = $data.getTemplateInfo())
$!template.setLayoutTemplate("/Popup_empty.vm")

#set ($sessionId = $om.getLabel())

<script language="Javascript">

    var launchInProgress = false;

    function validate() {

        var scansSelected = true;
        var requiredFields = {
            scan_ids: 'input'
        };
        for (var fieldName in requiredFields) {
            var eleVal = [];
            if (requiredFields[fieldName] === 'checkbox') {
                eleVal = [];
                $('input[name=' + fieldName + ']:checked').each(function () {
                    eleVal.push($(this).val());
                });
                if (eleVal.length === 0) {
                    scansSelected = false;
                    break;
                }
            } else {
                eleVal = $(requiredFields[fieldName] + "[name=" + fieldName + "]").val()
                if (eleVal.length === 0) {
                    scansSelected = false;
                    break;
                }
            }

        }
        return scansSelected;
    }

    function noneClicked(noneObj) {
        var type = noneObj.getAttribute("class")
        if (noneObj.checked) {
            var checkedScans = $("input[name^="+type+"]:checked");
            var l = checkedScans.length;
            for (var i = 0; i < l; i++) {
                $(checkedScans[i]).prop("checked", false)
            }
        }
    }

    function scanClicked(scanObj) {
        var type = scanObj.getAttribute("class")
        if (scanObj.checked) {
            $("#x"+type).prop("checked", false)
        } else {
            var checkedScans = $("input[name^="+type+"]:checked");
            if (checkedScans && checkedScans.length > 0) {
                $("#x"+type).prop("checked", "checked")
            }
        }
    }


    /* page config */

    // set popup window title properly
    $(document).prop("title","Launch FreeSurfer Pipeline for $sessionId");

    $(document).ready(function(){
        // set display height and width according to popup size
        $("#pipeline_modal").width($(window).width());
        $("#pipeline_modal").height($(window).height());
        $("#pipeline_modal .body").height($(window).height()-$("#pipeline_modal .footer").height());

        // Show the DICOM / NIFTI selection if any NIFTI exist on the session
        var resources = [];
        #foreach ($label in $scanResourceLabelSet)
        resources.push("$label");
        #end
        if (resources.indexOf("NIFTI") > -1) {
            $("#format-param").removeClass("hidden");
        }

        set_scan_ids();

    });

    // toggle visibility of advanced parameters. (parameter values are always passed to form, whether or not they are visible)
    // $("#advanced-params-toggle").on("click",function(){
    //     var adv = $("#advanced-params");
    //     if ($(adv).hasClass("hidden")) {
    //         $(this).html("Hide");
    //         $(adv).removeClass("hidden");
    //     } else {
    //         $(this).html("Show");
    //         $(adv).addClass("hidden");
    //     }
    // });
    function advanced_params_toggle(){
        var adv = $("#advanced-params");
        if ($(adv).hasClass("hidden")) {
            $(this).html("Hide");
            $(adv).removeClass("hidden");
        } else {
            $(this).html("Show");
            $(adv).addClass("hidden");
        }
    }

    function set_scan_ids() {
        var clickedVals = [];
        $('input.mprs-clicker:checked').each(function(){
            clickedVals.push($(this).val());
        });
        $('input[name=scan_ids]').val(clickedVals.join());
    }

    function confirm() {
        xmodal.confirm({
            content: "Run FreeSurfer pipeline with these parameters?",
            cancelLabel: "Cancel",
            cancelAction: "Return",
            okAction: launch
        });
    }

    function popupCloser(){
        // reload the session window that opened this popup, then close the popup
        window.opener.location.reload();
        self.close();
    }

    function launch(){
        var valid = validate();
        if (valid && !launchInProgress) {
            launchInProgress = true;

            // add brackets around scan id list
            var scanIdList = $('input[name=scan_ids]').val();
            $('input[name=scan_ids]').val("["+scanIdList+"]");
            // if (scanIdList.indexOf('[') == -1 && scanIdList.indexOf(']') == -1) {
            //    $('input[name=scan_ids]').val("["+scanIdList+"]");
            // }


            // collect all form parameters and submit to pipeline handler
            var params = $('form[name=FreesurferBuildOptions]').serialize();
            XNAT.xhr.ajax({
                url: XNAT.url.csrfUrl('/REST/projects/$project/pipelines/Freesurfer_5.3/experiments/$om.getId()'),
                method: 'POST',
                data: params,
                contentType: 'application/x-www-form-urlencoded',
                done: function(){
                    launchInProgress = false;
                },
                fail: function(e){
                    xmodal.alert("Something went wrong. Message: " + e.statusText)
                },
                success: function(){
                    xmodal.message({
                        content: "FreeSurfer pipeline successfully queued.",
                        okAction: popupCloser
                    });
                }
            });
        } else if (!valid) {
            xmodal.message("Required fields are empty. Please check your inputs.");
        }
    }



</script>

<!-- create an xmodal container inside the popup window -->
<div id="pipeline_modal" class="xmodal fixed embedded open" style="max-width: 100%; max-height: 100%; margin: 0;">
    <div class="body content scroll">
        <!-- handle error messages -->
        #if ($data.message)
        <div class="error">$data.message</div>
        #end

        <div class="inner">
        <!-- modal body start -->

        <form name="FreesurferBuildOptions" method="post" onsubmit="return validate();" class="optOutOfXnatDefaultFormValidation">
            <h2>Set FreeSurfer Execution Parameters for $sessionId</h2>

            #if ($projectParameters.get("use_manual_qc").getCsvvalues()=="1")
            <!-- display and create an input for the preselected "best" scan -->

                #set ($bestScanError = true)
                <fieldset>
                    <h3>Looking for Predetermined "Best Scan"</h3>
                    <p><em>Scan selection is automated for this project, based on the results of Manual QC.</em></p>
                    #foreach ($scan in $mrScans)
                        #if ($bestScans.contains($scan.getId()))
                            <p><label><input type="checkbox" checked value="$scan.Id" class="scan mprs-clicker" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>
                            #set ($bestScanError = false)
                        #else
                            <p><label><input type="checkbox" value="$scan.Id" class="scan mprs-clicker" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>
                        #end
                    #end
                    <input name="scan_ids" value="[]" type="hidden" />
                </fieldset>

                #if ($bestScanError==true)
                    <script>
                        disableNext();
                    </script>
                    <fieldset class="error">
                        <legend>Looking for Predetermined "Best Scan"</legend>
                        <p><strong>ERROR: Could not select scan for pipeline. Please run (or re-run) Manual QC and determine a "Best Scan" for this session.</strong></p>
                    </fieldset>
                #end

            #else
            <!-- get users to determine which scan to use. Ignore any unusable scans -->
                <fieldset>
                    <h3>Determine best scan to use</h3>

                    #foreach ($scan in $mrScans)
                        #if ($scan.getQuality().equalsIgnoreCase("usable"))
                            #set ($attrib="checked")
                        #else
                            #set ($attrib="")
                        #end
                        <p><label><input type="checkbox" value="$scan.Id" $attrib onclick="validate()" class="scan mprs-clicker" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                    #end
                    <input name="scan_ids" value="" type="hidden" />
                </fieldset>

            #end
            <script>
                $('input.mprs-clicker').on('click',function(){
                    set_scan_ids();
                });
            </script>

            <!-- #foreach ($scanListWithNames in [[$t2Scans,"T2","t2"],[$flairScans,"FLAIR","flair"]])
                #set ($scanList=$scanListWithNames.get(0))
                #set ($longName=$scanListWithNames.get(1))
                #set ($shortName=$scanListWithNames.get(2))
                #set ($sn=$shortName)
                #if (!$scanList.isEmpty())
                    <fieldset>
                        <legend>Select $longName scan</legend>

                        <p><label for="x$sn"><input type="checkbox" class="$sn" name="x$sn" id="x$sn" checked onclick="noneClicked(this)">None</label></p>
                        #set ($j = 0)
                        #foreach ($scan in $mrScans)
                            #if ($bestScans.contains($scan.getId()))
                                <p><label for="${sn}_$j"><input type="checkbox" name="${sn}_$j" id="${sn}_$j" checked value="$scan.Id" class="scan" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>
                            #else
                                <p><label for="${sn}_$j"><input type="checkbox" name="${sn}_$j" id="${sn}_$j" value="$scan.Id" class="scan" data-format="$allScanResourceLabels.get($scan.Id)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>
                            #end
                            #set ($j = $j + 1)
                        #end
                    </fieldset>
                    <input type="hidden" name="${sn}_rowcount" id="${sn}_rowcount" value="$j">
                #end
            #end -->

            <fieldset>
                <p>Advanced Parameters (<a href="javascript:" id="advanced-params-toggle" onclick="advanced_params_toggle()">Hide</a>)</p>
                <div id="advanced-params">
                    <p><label for="recon_all_args" title="Arguments to pass to recon-all">recon-all args</label>
                    #set($recon_all_args=$projectParameters.get("recon_all_args").csvvalues)
                    <input type="text" name="recon_all_args" id="recon_all_args" value="$recon_all_args" size="75" /></p>

                    <div class="hidden" id="format-param">
                        <p><label for="format" title="File type to pass to recon-all">Input data file type</label>
                        <input type="radio" name="format" id="format" value="DICOM" checked>DICOM</input>
                        <input type="radio" name="format" id="format" value="NIFTI">NIFTI</input>
                        </p>
                    </div>
                </div>
            <p></p>

            </fieldset>

        </form>

        <!-- modal body end -->
        </div>
    </div>
    <div class="footer">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="buttons">
                <a id="button-advance" class="default button panel-toggle" href="javascript:" onclick="confirm()">Submit Pipeline</a>
                <a class="cancel close button" href="javascript:self.close()">Cancel</a>
            </span>
        </div>
    </div>
</div>
<!-- end of xmodal container -->
